<?php get_header(); ?>
<style>
    #myCarousel div.carousel-item {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/large city.jpg");
    }
</style>
<div id="CareersPage">
    <div id="myCarousel" class="firstLevelDiv">
        <div class="container-fluid">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                    </div>
                    <div class="carousel-item">
                    </div>
                    <div class="carousel-item">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div id="franchises" class="firstLevelDiv">
        <div class="container">
            <div class="section_title">
                <h2 class="myNarrowFont text-center"></h2>
                <h2 class="myNarrowFont myBold text-left">Careers</h2>
            </div>

            <p>
                Restaurants may be classified or distinguished in many different ways. The primary factors are usually the food itself (e.g. vegetarian, seafood, steak); the cuisine (e.g. Italian, Chinese, Japanese, Indian, French, Mexican, Thai) and/or the style of offering (e.g. tapas bar, a sushi train, a tastet restaurant, a buffet restaurant or a yum cha restaurant). Beyond this, restaurants may differentiate themselves on factors including speed (see fast food), formality, location, cost, service, or novelty themes (such as automated restaurants).
            </p>
        </div>
    </div>

    <div id="careersRecent" class="firstLevelDiv careers">
        <div class="container">
            <div>
                <span class="myNarrowFont myBold">RECENT JOB OPENINGS</span>
            </div>
            <div class="row">
                <!-- THIS IS THE BEGINING OF THE LOOP -->
                <?php

                $args = array(
                    'posts_per_page' => '5',
                    'category_name' => 'careers',
                );

                $query = new WP_Query($args);

                if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>

                    <!-- THIS IS THE BEGINING OF THE LOOP -->

                    <article class="col-lg-4 recognition_article centerFlex flex-column">

                        <a href="<?php echo get_permalink(); ?>">
                            <h2 class="recognition_h2"><?php echo the_time('F Y'); ?></h2>
                            <h1 class="recognition_h1"><?php the_title(); ?></h1>
                            <p class="recognition_p">
                                <?php the_excerpt(); ?>
                            </p>
                        </a>

                    </article>

                    <!-- THIS IS THE END OF THE LOOP -->
                <?php endwhile; else  : ?>
                    <?php echo wpautop('No posts'); ?>

                <?php endif; ?>

                <!-- THIS IS THE END OF THE LOOP -->
                <article class="col-lg-4">
                    <hr>
                    <a href="<?php echo get_page_link(234); ?>"
                    class="btn btn-secondary btn-lg active centerFlex flex-column" role="button" aria-pressed="true">
                        SEE ALL OPPORTUNITIES
                    </a>
                    <hr>
                </article>
            </div>
        </div>
    </div>
</div><!--id="CareersPage"-->
<?php get_footer(); ?>
