<?php get_header(); ?>
<style>
    #careersNonCarouselCover {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/covers/515884946.jpg");
    }
</style>

<div id="allCareersPage">

    <div class="firstLevelDiv">
        <div id="careersNonCarouselCover" class="container-fluid">
            <span>CAREER OPPORTUNITIES</span>
        </div>
    </div>
    <!--************* CAREERS 1 **********************-->
    <div id="careers1" class="firstLevelDiv careers">
        <div class="container">
            <div>
                <h1 class="myNarrowFont myBold">CAREERS 1</h1>
            </div>
            <div class="row">
                <!-- THIS IS THE BEGINING OF THE LOOP -->
                <?php

                $args = array(
                    'category_name' => 'careers1',
                );

                $query = new WP_Query($args);

                if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>

                    <!-- THIS IS THE BEGINING OF THE LOOP -->

                    <article class="col-lg-3 recognition_article text-center">

                        <a href="<?php echo get_permalink(); ?>">
                            <h2 class="recognition_h2"><?php echo the_time('F Y'); ?> – Toronto, Ontario</h2>
                            <h1 class="recognition_h1"><?php the_title(); ?></h1>
                        </a>

                    </article>

                    <!-- THIS IS THE END OF THE LOOP -->
                <?php endwhile; else  : ?>
                    <?php echo wpautop('No posts'); ?>

                <?php endif; ?>

                <!-- THIS IS THE END OF THE LOOP -->
            </div>
        </div>
    </div>
    <!--************* CAREERS 2 **********************-->
    <div id="careers2" class="firstLevelDiv careers">
        <div class="container">
            <div>
                <h1 class="myNarrowFont myBold">CAREERS 2</h1>
            </div>
            <div class="row">
                <!-- THIS IS THE BEGINING OF THE LOOP -->
                <?php

                $args = array(
                    'category_name' => 'careers2',
                );

                $query = new WP_Query($args);

                if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>

                    <!-- THIS IS THE BEGINING OF THE LOOP -->

                    <article class="col-lg-3 recognition_article text-center">

                        <a href="<?php echo get_permalink(); ?>">
                            <h2 class="recognition_h2"><?php echo the_time('F Y'); ?> – Toronto, Ontario</h2>
                            <h1 class="recognition_h1"><?php the_title(); ?></h1>
                        </a>

                    </article>

                    <!-- THIS IS THE END OF THE LOOP -->
                <?php endwhile; else  : ?>
                    <?php echo wpautop('No posts'); ?>

                <?php endif; ?>

                <!-- THIS IS THE END OF THE LOOP -->
            </div>
        </div>
    </div>
    <!--************* CAREERS 3 **********************-->
    <div id="careers3" class="firstLevelDiv careers">
        <div class="container">
            <div>
                <h1 class="myNarrowFont myBold">CAREERS 3</h1>
            </div>
            <div class="row">
                <!-- THIS IS THE BEGINING OF THE LOOP -->
                <?php

                $args = array(
                    'category_name' => 'careers3',
                );

                $query = new WP_Query($args);

                if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>

                    <!-- THIS IS THE BEGINING OF THE LOOP -->

                    <article class="col-lg-3 recognition_article text-center">

                        <a href="<?php echo get_permalink(); ?>">
                            <h2 class="recognition_h2"><?php echo the_time('F Y'); ?> – Toronto, Ontario</h2>
                            <h1 class="recognition_h1"><?php the_title(); ?></h1>
                        </a>

                    </article>

                    <!-- THIS IS THE END OF THE LOOP -->
                <?php endwhile; else  : ?>
                    <?php echo wpautop('No posts'); ?>

                <?php endif; ?>

                <!-- THIS IS THE END OF THE LOOP -->
            </div>
        </div>
    </div>



    <!--************* CAREERS 3 **********************-->

</div>

<?php get_footer(); ?>
