<?php get_header(); ?>
<style>
    #careersNonCarouselCover {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/covers/517010386.jpg");
    }
</style>

<div id="allCareersPage">

    <div class="firstLevelDiv">
        <div id="careersNonCarouselCover" class="container-fluid">
            <span>Partnerships</span>
        </div>
    </div>
    <!--************* SINGLE-COLUMN BUNCH OF TEXT **********************-->
    <div id="careers1" class="firstLevelDiv careers">
        <div class="container">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi doloremque eum ipsum optio pariatur saepe, sequi similique. Cumque exercitationem harum quasi quod saepe! Accusantium ad, at atque cumque, debitis deserunt dicta dignissimos doloribus eius hic id impedit ipsa ipsam ipsum laboriosam maxime modi neque nesciunt, nisi obcaecati omnis possimus quaerat quas reprehenderit repudiandae sint sit unde veritatis vitae voluptates. Ab at blanditiis corporis delectus dolore dolores ea et, eum expedita fugiat impedit in ipsam magni mollitia, natus nesciunt nihil numquam odit, perspiciatis provident quaerat quidem quis recusandae rerum sed sunt tempora tenetur voluptas voluptate voluptatum. Consequuntur debitis excepturi, fuga illum impedit magnam, minima minus omnis perferendis quas quibusdam reiciendis sapiente vel! Accusantium cupiditate delectus dicta ducimus eligendi est harum natus nemo, neque nulla pariatur quidem, reprehenderit saepe ullam voluptatem? Accusamus alias aliquid at consectetur consequuntur cupiditate, distinctio dolore dolorem doloremque dolorum eaque eligendi esse, ex, excepturi illo in inventore laudantium maxime modi nemo nesciunt nobis nulla odit perferendis quasi quia quos repudiandae sit soluta sunt suscipit tenetur veritatis voluptatibus? Alias, assumenda beatae deleniti expedita, id modi molestiae officiis possimus quia ratione rem sequi sint ullam, vero voluptas? Ab autem, dicta distinctio dolor dolores, expedita, fugit molestias natus quas quos repudiandae tenetur. Neque obcaecati optio perspiciatis repudiandae sequi. Ab, accusantium at aut blanditiis cumque cupiditate ex excepturi, id ipsum iure molestias nobis non provident quasi quisquam rerum sed velit voluptas! Aperiam cupiditate earum enim esse exercitationem fugiat fugit illum inventore maiores nihil, sunt velit voluptate. Aliquid debitis esse id ipsum iste, laudantium, minus necessitatibus nemo nihil placeat praesentium quae quam quas quia rerum saepe sed similique voluptatem voluptates voluptatibus. Assumenda blanditiis, cumque, debitis deleniti eos esse eveniet facilis fugiat illum nihil nobis quos veniam? A animi, aut blanditiis consequuntur culpa cumque dignissimos ducimus esse excepturi facilis in magni maiores molestiae nam necessitatibus nemo nesciunt nobis quae ratione rem repellat repellendus vel voluptatibus. Ab error fuga fugit id itaque omnis quam! Adipisci aliquid assumenda aut blanditiis corporis cupiditate deleniti deserunt dignissimos, distinctio esse eveniet ipsa laborum molestiae natus neque odio placeat, praesentium quam recusandae rem saepe vitae voluptatum. Doloribus facere magnam magni necessitatibus quas sit vel. Ad aliquid cupiditate doloremque excepturi facilis fuga fugiat nihil odio provident qui, recusandae repudiandae sapiente, sint temporibus unde. Accusamus aliquam consequatur doloremque illo impedit incidunt, ipsam ipsum, maiores nemo neque nisi nostrum odio quidem quo rerum unde voluptas voluptatem! Beatae facere fugiat iusto pariatur sed velit! Ad alias aliquid aperiam corporis culpa deleniti deserunt dicta dolore dolores eos error esse explicabo hic in laboriosam magni maiores, minima nesciunt nisi nostrum nulla numquam odit officiis perferendis perspiciatis possimus ratione rem reprehenderit saepe sint soluta suscipit tempora tempore totam veritatis vitae voluptates. Assumenda, commodi distinctio dolore doloremque illum iste iure maxime nihil perspiciatis possimus, quasi quis repudiandae, saepe sequi sit tempore velit voluptates! Asperiores dignissimos ducimus esse ipsam maiores molestiae pariatur praesentium qui, quia quo tempora, tempore. Amet cupiditate ea earum, esse facilis laborum magni maxime nihil odit, officiis optio perspiciatis praesentium quaerat reiciendis repellat repellendus suscipit. Atque!
            </p>
        </div>
    </div>

</div>

<?php get_footer(); ?>
