<?php

function custom_excerpt_length( $length ) {
    return 20;
}

function register_my_menu() {
    register_nav_menu(
        'header-menu',__( 'Header Menu' )
    );
}




add_action( 'init', 'register_my_menu' );

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_theme_support( 'post-thumbnails' );