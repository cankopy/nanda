<div id="myFooter" class="firstLevelDiv">
    <div class="container">

        <div class="row">
            <div id="f1" class="col-sm-4">
                <ul class="">
                    <li class="nav-link">
                        <a class="" href="#">About</a>
                    </li>
                    <li class="nav-link">
                        <a class="" href="#">Our Brands</a>
                    </li>
                    <li class="nav-link">
                        <a class="" href="#">Restaurant Locations</a>
                    </li>
                </ul>
            </div>
            <div id="f2" class="col-sm-4">
                <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/Nanda-logo-01.png" class="img-fluid">
                </a>
                <h3>
                    Copyright &copy; 2017 NANDA HOLDINGS
                </h3>
            </div>
            <div id="f3" class="col-sm-4">
                <ul>
                    <li class="nav-link">
                        <a class="" href="#">Training</a>
                    </li>
                    <li class="nav-link">
                        <a class="" href="#">Partnership opportunities</a>
                    </li>
                    <li class="nav-link">
                        <a class="" href="#">Contact</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- jQuery first, then Tether, then Bootstrap 4 JS. -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/nanda_effects.js"></script>
<?php wp_footer(); ?>
</body>
</html>