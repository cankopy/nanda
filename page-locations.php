<!--I am the blog posts page.-->
<?php get_header(); ?>
<style>

</style>
<main id="locationsPage" class="container-fluid">

    <!--GOOGLE MAPS-->
    <div id="map">
    </div>

    <!--LISTINGS FIRST SECTION (ID = SWISS)-->
    <section class="storeBrandMapListing row" id="swiss">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(1).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>
        </div>
    </section><!-- END (ID = SWISS)-->
    <section class="storeBrandMapListing row" id="harveys">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(5).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>
        </div>
    </section><!-- END (ID = HARVEYS)-->
    <section class="storeBrandMapListing row" id="montanas">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(7).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>
        </div>
    </section><!-- END (ID = MONTANAS)-->
    <section class="storeBrandMapListing row" id="kelseys">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(6).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <!--<div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/8.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #8 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>-->
        </div>
    </section><!-- END (ID = KELSEYS)-->
    <section class="storeBrandMapListing row" id="eastside">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(3).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <!--<div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/8.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #8 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>-->
        </div>
    </section><!-- END (ID = EASTSIDE)-->
    <section class="storeBrandMapListing row" id="fionnmaccools">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(4).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <!--<div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/8.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #8 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>-->
        </div>
    </section><!-- END (ID = EASTSIDE)-->
    <section class="storeBrandMapListing row" id="darcymcgees">
        <div class="img_storeBrandMapListing col-md-2">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(2).png">
        </div>
        <div class="row col-md-10">
            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/1.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #1 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/2.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #2 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/3.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #3 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/4.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #4 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/5.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #5 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/6.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #6 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>

            <div class="individualStore col-md-3">
                <div id="box">
                    <h1 id="popupTitle_id" class="popupTitle_class">
                        <span>
                            <img src="http://maps.google.com/mapfiles/kml/paddle/7.png">
                        </span>
                        <span>
                            Swiss Chalet <a href="#"> #7 </a>
                        </span>
                    </h1>
                    <p id="complete_address">
                        124 Roadina Road, Mississauga ON L1L1L1, Canada
                    </p>
                    <p id="phone">
                        T. <span id="phone1">905.000.0000</span>
                        <br>
                        T. <span id="phone2">905.000.0000</span>
                    </p>
                    <div id="emptyspace">

                    </div>
                </div>
            </div>
        </div>
    </section><!-- END (ID = EASTSIDE)-->
</main>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWhHMDKk6ucxd1YBiOjFgT0D2MmRj3Fs0&callback=initMap" async defer>
</script>
<script src="<?php echo get_template_directory_uri(); ?>/js/myGMaps.js" type="application/javascript">
</script>
<?php get_footer(); ?>
