<!--For posts viewed individually-->
<?php get_header(); ?>

<div id="individualPost">

    <?php /*THIS IS THE BEGINING OF THE LOOP*/
    $args = array(
        'category_name' => 'careers'
    );
    $query = new WP_Query($args);
    ?>

    <?php if ( $query -> have_posts() ) : ?>
        <style>
            #careersNonCarouselCover {
                background-image: url("<?php echo get_template_directory_uri(); ?>/images/covers/515884946.jpg");
            }
        </style>
        <div class="firstLevelDiv">
            <div id="careersNonCarouselCover" class="container-fluid">
                <span>CAREER OPPORTUNITIES</span>
            </div>
        </div>

        <?php wp_reset_postdata(); ?>
    <?php else  : ?>
        <h1 class="alert-danger">NOTHING TO SHOW YOU!</h1>
    <?php endif; ?>
    <!-- THIS IS THE END OF THE LOOP -->

    <!-- THIS IS THE BEGINNING OF THE LOOP -->
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <article class="single_post container">
            <?php /*if(has_post_thumbnail() ) : */?><!--
                <div class="post-tb container-fluid">
                    <?php /*the_post_thumbnail(); */?>
                </div>
            --><?php /*endif; */?>
            <div class="post_body">

                <div id="postTitle">

                    <span class="post_title">
                        <?php the_title(); ?>
                    </span>
                    <span class="post_date">
                        <?php echo the_time('F Y'); ?>
                    </span>

                </div>

                <div class="post_content">
                    <?php the_content(); ?>
                </div>

            </div>

        </article>

        <!-- THIS IS THE END OF THE LOOP -->
    <?php endwhile; else  : ?>

    <?php endif; ?>
    <!-- THIS IS THE END OF THE LOOP -->
    <div class="container text-center">
        <hr>
        <h2 class="resume">Click here to send your cover letter and resume</h2>
        <button type="button" class="btn btn-secondary disabled">Contact</button>
    </div>
</div>

<?php get_footer(); ?>