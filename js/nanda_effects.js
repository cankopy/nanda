$(function () {
    /*On page load function*/
})

$("#ourBrands img").hover(

    function() {
        $( "#ourBrands img" ).css( "opacity", "0.2" );
        $( this ).css( "opacity", "1" );
    },

    function() {
        /*no need for callback*/
        $( "#ourBrands img" ).css( "opacity", "1" );
    }

);