var p = document.getElementById("p");

/*since f1-f2 returns a #promise, we can use #then */
f1(0).then(imTheResult => {
    p.innerHTML += "<br>(" + imTheResult + ") f1 is slower (total = 4s to output)";
});

f2(0).then(imTheResult => {
    p.innerHTML += "<br>(" + imTheResult + ") f2 is faster (total = 2s to output)";
});

function resolveAfter2Seconds(x) {
    return new Promise(resolve => {
        /*ref. setTimeout(function, milliseconds, param1, param2, ...)*/
        setTimeout(
            () => resolve(x),2000
        );
    });
}

async function f1(x) {
    var a = await resolveAfter2Seconds(10);
    var b = await resolveAfter2Seconds(1);
    return x + a + b;
}
async function f2(x) {
    var a = resolveAfter2Seconds(100);
    var b = resolveAfter2Seconds(10);
    return x + await a + await b;
}

