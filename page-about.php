<?php get_header(); ?>
<style>
    #careersNonCarouselCover {
        background-image: url("<?php echo get_template_directory_uri(); ?>/images/covers/517010386.jpg");
    }
</style>

<div id="aboutPage">

    <div class="firstLevelDiv">
        <div id="careersNonCarouselCover" class="container-fluid">
            <span>About Nanda Holdings</span>
        </div>
    </div>
    <!--************* SINGLE-COLUMN BUNCH OF TEXT **********************-->
    <div id="careers1" class="firstLevelDiv careers">
        <div class="container">
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium alias aliquam aspernatur atque commodi dolor eligendi et fugit illo laborum nemo, nostrum obcaecati perspiciatis quaerat suscipit, velit. Commodi distinctio eaque eos est magni maxime minima quaerat rem sed veniam. Culpa dolor doloremque molestias quis quisquam reiciendis repellat voluptate. Ab accusantium atque beatae corporis cum, deserunt distinctio facilis hic id ipsum laboriosam nisi, officiis quaerat quia sed suscipit veniam voluptates? Assumenda ea enim ipsam iusto magnam pariatur ratione recusandae reprehenderit soluta ut. Ab aliquam architecto, consectetur eveniet explicabo fugit incidunt itaque laboriosam magnam, magni necessitatibus neque officia quae quia quisquam recusandae repudiandae tempore, temporibus veniam voluptatum. A aliquid culpa dignissimos enim et fuga, id laudantium, minima molestias omnis qui ratione sed! Ab accusamus adipisci at beatae commodi cumque delectus eveniet maxime necessitatibus nemo nesciunt nisi numquam obcaecati quasi quidem saepe sequi, sint voluptatibus! Cum harum impedit repellat. Eligendi exercitationem explicabo fuga, fugit iure laboriosam, molestiae nesciunt optio perferendis perspiciatis, possimus quaerat reiciendis ut. Deleniti dolores fugit harum ipsam libero mollitia quam saepe. Alias aliquam consectetur, dolore eum fugit illo iusto molestiae nam, perspiciatis quam quas quasi quidem ratione sed tenetur, vel veritatis voluptatibus! Ad amet, at. Dolor fugiat minima vero.
            </p>
        </div>
        <div class="container-fluid text-center">
            <img src="<?php echo get_template_directory_uri(); ?>/images/ABOUT-EXAMPLE.PNG" class="img-responsive">
        </div>
    </div>

</div>

<?php get_footer(); ?>