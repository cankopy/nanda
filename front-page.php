<?php get_header(); ?>

    <div id="myCarousel" class="firstLevelDiv">
        <div class="container-fluid">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">

                    </div>
                    <div class="carousel-item">

                    </div>
                    <div class="carousel-item">

                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

    <div id="ourBrands" class="firstLevelDiv">
        <div class="container">
            <div>
                <span class="myNarrowFont myBold">Explore</span> <span class="myFont">our brands</span>
            </div>
            <div class="row">
                <div class="col-lg-1 offset-lg-1">
                    <a href="//www.swisschalet.com/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(1).png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-2">
                    <a href="//www.harveys.ca/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(5).png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-1">
                    <a href="//www.montanas.ca/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(7).png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-1">
                    <a href="//www.kelseys.ca/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(6).png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-1">
                    <a href="//www.eastsidemarios.com/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(3).png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-2">
                    <a href="//www.fionnmaccools.com/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(4).png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg-2">
                    <a href="//www.darcymcgees.com/" target="_blank">
                        <img
                                src="<?php echo get_template_directory_uri(); ?>/images/logos/logos%20(2).png" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div id="franchises" class="firstLevelDiv">
        <div class="container">
            <div class="section_title">
                <h2 class="myNarrowFont text-center">Owner and Operator of</h2>
                <h2 class="myNarrowFont myBold">Restaurant Franchises</h2>
            </div>

            <p>
                Restaurants may be classified or distinguished in many different ways. The primary factors are usually the food itself (e.g. vegetarian, seafood, steak); the cuisine (e.g. Italian, Chinese, Japanese, Indian, French, Mexican, Thai) and/or the style of offering (e.g. tapas bar, a sushi train, a tastet restaurant, a buffet restaurant or a yum cha restaurant). Beyond this, restaurants may differentiate themselves on factors including speed (see fast food), formality, location, cost, service, or novelty themes (such as automated restaurants).
            </p>
            <div class="row">

                <article class="col-lg-4 restaurant_article centerFlex flex-column">

                        <img class="restaurant_img"
                             src="<?php echo get_template_directory_uri(); ?>/images/fkicon1.PNG">

                        <h1 class="restaurant_h1">
                            Restaurants
                        </h1>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cumque deserunt doloremque doloribus dolorum earum enim eos esse harum itaque, magnam minus obcaecati placeat ratione reiciendis, repellendus sunt veritatis vero!
                        </p>

                </article>

                <article class="col-lg-4 restaurant_article centerFlex flex-column">

                    <img class="restaurant_img"
                         src="<?php echo get_template_directory_uri(); ?>/images/fkicon2.PNG">

                    <h1 class="restaurant_h1">
                        Million Annually
                    </h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cumque deserunt doloremque doloribus dolorum earum enim eos esse harum itaque, magnam minus obcaecati placeat ratione reiciendis, repellendus sunt veritatis vero!
                    </p>

                </article>

                <article class="col-lg-4 restaurant_article centerFlex flex-column">
                    <img class="restaurant_img"
                         src="<?php echo get_template_directory_uri(); ?>/images/fkicon3.PNG">

                    <h1 class="restaurant_h1">
                        Employees
                    </h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cumque deserunt doloremque doloribus dolorum earum enim eos esse harum itaque, magnam minus obcaecati placeat ratione reiciendis, repellendus sunt veritatis vero!
                    </p>
                </article>

            </div>
        </div>
    </div>

    <div id="recognition" class="firstLevelDiv">
        <div class="container">
            <div class="section_title">
                <h2 class="myNarrowFont myBold">Industry recognition</h2>
            </div>

            <div class="row">

                <!-- THIS IS THE BEGINING OF THE LOOP -->
                <?php

                $args = array(
                    'posts_per_page' => '3',
                    'category_name' => 'c_recognition',
                );

                $query = new WP_Query($args);

                if ( $query -> have_posts() ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>

                    <!-- THIS IS THE BEGINING OF THE LOOP -->

                    <article class="col-lg-4 recognition_article centerFlex flex-column">

                        <a href="<?php echo get_page_link(81); ?>">
                            <h2 class="recognition_h2"><?php echo the_time('F Y'); ?></h2>
                            <h1 class="recognition_h1"><?php the_title(); ?></h1>
                            <p class="recognition_p">
                                <?php the_excerpt(); ?>
                            </p>
                        </a>

                    </article>
                    <!-- THIS IS THE END OF THE LOOP -->
                <?php endwhile; else  : ?>
                    <?php echo wpautop('No posts'); ?>
                <?php endif; ?>
                <!-- THIS IS THE END OF THE LOOP -->

            </div>
        </div>
    </div>
<?php get_footer(); ?>