<?php get_header(); ?>
    <div class="container text-center" id="error">

        <?php /*THIS IS THE BEGINING OF THE LOOP*/
        $args = array(
        );
        $query = new WP_Query($args);
        ?>

        <?php if ( $query -> is_page( ) ) : while ( $query -> have_posts() ) : $query -> the_post(); ?>

            <h1 class="alert-success">GENERIC PAGE: <?php echo get_the_title(); ?></h1>

        <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else  : ?>
            <h1 class="alert-danger">GENERIC PAGE: NOTHING TO SHOW YOU!</h1>
        <?php endif; ?>
        <!-- THIS IS THE END OF THE LOOP -->
    </div>
<?php get_footer(); ?>