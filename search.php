<?php get_header(); ?>

<div class="container text-center" id="error">
    <div class="row">
        <div class="alert-success">
            <div class="main-icon text-success">SEARCH RESULTS</div>
            <h1>Future home of something quite cool.</h1>
        </div>
    </div>
</div>

<?php get_search_form(); ?>

<?php get_footer(); ?>
